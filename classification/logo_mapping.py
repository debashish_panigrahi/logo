import numpy as np
import os
import random
import re
from tqdm import tqdm
import pandas as pd
import cv2
import gc
from sklearn.preprocessing import LabelEncoder, Normalizer
from sklearn.cluster import KMeans
from sklearn.neighbors import NearestNeighbors


def generate_logo_images_from_pages(customer_id, df_ext_cust, logo_dir, i_range, n_chunk):
    logo_image_dir = os.path.join(logo_dir, '/classification/images')
    range_df_ext = range(df_ext_cust.shape[0])[i_range * n_chunk:(i_range + 1) * n_chunk]
    logos_file = logo_dir + f'/df_logo_{customer_id}_{i_range}.pkl'
    if os.path.exists(logos_file):
        df_logos = pd.read_pickle(logo_dir + f'/df_logo_{customer_id}_{i_range}.pkl')
        done_pages = set(df_logos['page_image_name_'])
        all_logos = [rl for _, rl in df_logos.iterrows()]
    else:
        done_pages = set()
        all_logos = []

    done_idxs = df_ext_cust.iloc[range_df_ext]['page_image_name_'].isin(done_pages).to_numpy().nonzero()[0]
    n_progress = done_idxs.max() if done_idxs.size else 0
    range_df_ext = range_df_ext[n_progress:]

    for idx in tqdm(range_df_ext):
        row = df_ext_cust.iloc[idx]
        image_page_dao = InvoiceImagePageDAO()
        msgkw = {k: row[k] for k in ['customer_id', 'invoice_id', 'image_uri', 'page_index']}
        msg = InvoiceExtractionMessage(**msgkw)
        page_info = msg.get_image_page_info()
        page = image_page_dao.get_image_page(page_info)
        page_df = get_df(page)
        logos = LogoPrediction.get_logo_descriptions(page.png.value, page_df, row['customer_id'])
        img = cv2.imdecode(np.frombuffer(page.png.value, dtype=np.uint8),
                           cv2.IMREAD_COLOR)
        logo_dir = os.path.join(logo_image_dir, row['supplier_name_'])
        if not os.path.exists(logo_dir):
            os.makedirs(logo_dir)
        for il, logo in enumerate(logos):
            rlogo = pd.Series()
            rlogo['x0'], rlogo['y0'], rlogo['x1'], rlogo['y1'] = logo.bbox.x0, logo.bbox.y0, logo.bbox.x1, logo.bbox.y1
            rlogo['logo_description'] = logo.logo_description
            for key in ['customer_id', 'supplier_id', 'invoice_id', 'page_index', 'supplier_name_',
                        'supplier_name', 'page_image_name_']:
                rlogo[key] = row[key]
            all_logos.append(rlogo)
            logo_path = os.path.join(logo_dir, row['page_image_name_'] + '_' + str(il) + '.png')
            cv2.imwrite(logo_path, img[int(rlogo['y0']):int(rlogo['y1']), int(rlogo['x0']):int(rlogo['x1']), :])

        if idx % 100 == 0:
            pd.DataFrame(all_logos).to_pickle(logo_dir + f'/df_logo_{customer_id}_{i_range}.pkl')
            gc.collect()


def generate_logo_graph(df_logo_embedding_train, prec_dist, max_dist):
    df_centroids = []
    for sup_id, df_sup_emb in tqdm(df_logo_embedding_train.groupby('supplier_id')):
        # drop duplicates to remove redundant points
        df_sup_emb = df_sup_emb.drop_duplicates()
        kmeans = KMeans(n_clusters=min(10, df_sup_emb.shape[0]), random_state=0).fit(df_sup_emb[list(range(128))])
        df_sup_centroids = pd.DataFrame(kmeans.cluster_centers_)
        df_sup_centroids['supplier_id'] = sup_id
        df_sup_centroids['customer_id'] = df_sup_emb['customer_id'].iloc[0]
        df_sup_centroids['supplier_name'] = df_sup_emb['supplier_name'].iloc[0]
        df_centroids.append(df_sup_centroids)
    df_centroids = pd.concat(df_centroids, sort=False, ignore_index=True)
    logo_neighbors_graph = NearestNeighbors(n_neighbors=30).fit(df_centroids[list(range(128))])
    logo_graph = {'logo_neighbors_graph': logo_neighbors_graph,
                    'centroids_df': df_centroids,
                    'prec_dist': prec_dist, 'max_dist': max_dist}
    return logo_graph


def get_test_train_features(logo_image_dir, suppliers=None, test_split=0.2):
    ximg, y_supplier = [], []
    for supplier in tqdm(os.listdir(logo_image_dir)):
        if suppliers is not None and supplier not in supplier:
            continue
        for logo_path in os.listdir(logo_image_dir+supplier):
            logo = cv2.imread(logo_image_dir+supplier+'/'+logo_path)
            y_supplier.append(supplier)
            ximg.append(logo)
    return np.array(ximg), np.array(y_supplier)





