import cv2
import numpy as np
import random
from tqdm import tqdm
import os
import itertools
import boto3
import pandas as pd


def import_plot():
    try:
        from matplotlib import pyplot as plt
        import matplotlib.patches as patches
        return plt
    except ImportError:
        return


def show_cells(img, cells):
    try:
        from matplotlib import pyplot as plt
        import matplotlib.patches as patches
    except ImportError:
        return
    colors = ['#377eb8', '#ff7f00', '#4daf4a', '#f781bf', '#a65628', '#984ea3', '#999999', '#e41a1c', '#dede00']
    fig, ax = plt.subplots(1, figsize=(20, 20))
    for idx, (x0, y0, x1, y1) in enumerate(cells):
        bb = np.array([[x0, y0], [x0, y1], [x1, y1], [x1, y0]])
        pol = patches.Polygon(bb, closed=True, fill=True, color=colors[idx % 8], alpha=.40)
        ax.add_patch(pol)
        font = min((y1 - y0) // 6, 15)
        lbl = ax.annotate(idx, xy=[x0, y0 + int(3 * font)], fontsize=font)
        lbl = ax.annotate(idx, xy=[x0, y1 - int(font)], fontsize=font)
        lbl = ax.annotate(idx, xy=[x1 - int(4 * font), y0 + int(3 * font)], fontsize=font)
        lbl = ax.annotate(idx, xy=[x1 - int(4 * font), y1 - int(font)], fontsize=font)
    axi = ax.imshow(img)
    plt.show()


def show_logos(img, logos, preds=None, sup=None):
    try:
        from matplotlib import pyplot as plt
        import matplotlib.patches as patches
    except ImportError:
        return
    colors = ['#377eb8', '#ff7f00', '#4daf4a', '#f781bf', '#a65628', '#984ea3', '#999999', '#e41a1c', '#dede00']
    fig, ax = plt.subplots(1, figsize=(20, 20))
    for idx, logo in enumerate(logos):
        bb = logo.bbox
        x0, y0, x1, y1, score = bb.x0, bb.y0, bb.x1, bb.y1, bb.ocr_confidence
        ploy = np.array([[x0, y0], [x0, y1], [x1, y1], [x1, y0]])
        pol = patches.Polygon(ploy, closed=True, fill=True, color=colors[idx % 8], alpha=0.4)
        ax.add_patch(pol)
        lbl1 = ax.annotate(round(score, 2), xy=[int(x0), int(y0)], fontsize=20)
        if preds is None:
            lbl2 = ax.annotate(logo.logo_description, xy=[int(x1), int(y1)], fontsize=15)
        else:
            if preds[idx]:
                df_logo = pd.DataFrame(preds[idx])
                src_logo = df_logo.apply(lambda r: '_'.join((r['pred_supplier'] + '_' + str(round(r['dist'], 2))).split('_')[1:]),
                              axis=1)
                des = '_'.join(list(src_logo)[:3])
                lbl2 = ax.annotate(des, xy=[int(x1), int(y1)], fontsize=15)
                lbl3 = ax.annotate(sup, xy=[int(x1), int(y0)], fontsize=15)

    axi = ax.imshow(img)
    plt.show()


def show_image(img):
    plt = import_plot()
    if plt is None:
        return
    fig, ax = plt.subplots(1, figsize=(10, 10))
    axi = ax.imshow(img)
    plt.show()


def gray(img):
    if len(img.shape) == 3:
        return cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    else:
        return img


def inverse(img, kernel_size):
    return 255 - img


def dialated(img, kernel_size):
    kernel_size = (1, 1) if kernel_size[0] > 1 else kernel_size
    return cv2.dilate(img, np.ones(kernel_size, np.uint8), iterations=1)


def bloated(img, kernel_size):
    kernel_size = (1, 1) if kernel_size[0] > 1 else kernel_size
    return cv2.erode(img, np.ones(kernel_size, np.uint8), iterations=1)


def bloated2(img, kernel_size):
    kernel_size = (1, 1) if kernel_size[0] > 1 else kernel_size
    return cv2.morphologyEx(img, cv2.MORPH_OPEN, np.ones(kernel_size, np.uint8))


def bubble_font(img, kernel_size):
    edges_high_thresh = cv2.Canny(img, 60, 120)
    return 255 - cv2.dilate(edges_high_thresh, np.ones((7,7), np.uint8), iterations=1)


def erode(img, kernel_size):
    return cv2.erode(img, np.ones(kernel_size, np.uint8), iterations=1)


def eroded2(img, kernel_size):
    return cv2.morphologyEx(img, cv2.MORPH_CLOSE, np.ones(kernel_size, np.uint8))


def get_ratio(img_gray):
    col1 = (img_gray < 125).sum()
    col2 = (img_gray > 125).sum()
    return min(col1, col2) / max(col1, col2)


def binarize(img, is_bubble_font):
    gray_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    rand = random.random()
    if rand < 0.25 and not is_bubble_font:
        gray_img = cv2.adaptiveThreshold(gray_img, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C,
                                     cv2.THRESH_BINARY, 11, 2)
    elif rand < 0.25 and not is_bubble_font:
        gray_img = cv2.adaptiveThreshold(gray_img, 255, cv2.ADAPTIVE_THRESH_MEAN_C,
                                         cv2.THRESH_BINARY, 11, 20)
    else:
        pass
        # _, gray_img = cv2.threshold(img, 127, 255, cv2.THRESH_BINARY)
        # if (gray_img > 128).sum() / (gray_img.shape[0] * gray_img.shape[1]) < 0.5:
        #     gray_img = 255 - gray_img
        # gray_img = cv2.cvtColor(gray_img, cv2.COLOR_BGR2GRAY)
    return cv2.cvtColor(gray_img, cv2.COLOR_GRAY2RGB)


def reshape_logo(new_logo, new_dim=160, center=False):
    max_dim = max(new_logo.shape[:2])
    reshaped_logo = np.ones((max_dim, max_dim, 3))
    if new_logo.max()-new_logo.min() > 1.1:
        reshaped_logo *= 255.0
    if not center:
        pad = random.randint(0, max_dim-min(new_logo.shape[:2]))
    else:
        pad = int((max_dim - min(new_logo.shape[:2])) // 2)

    pad0 = pad if new_logo.shape[0] < max_dim else 0
    pad1 = pad if new_logo.shape[1] < max_dim else 0
    reshaped_logo[pad0:pad0+new_logo.shape[0], pad1:pad1+new_logo.shape[1], :] = new_logo
    reshaped_logo = cv2.resize(reshaped_logo, (new_dim, new_dim))
    if new_logo.max()-new_logo.min() > 1.1:
        reshaped_logo = reshaped_logo.astype(np.int32)
    return reshaped_logo


def get_ratios(img_gray, n_windows=20):
    dim = int(np.sqrt((img_gray.shape[0]*img_gray.shape[1])/n_windows))
    img_ratios = []
    for i, j in list(itertools.product((range(img_gray.shape[0]//dim + 1)), (range(img_gray.shape[1]//dim + 1)))):
        img_ij = img_gray[i*dim:(i+1)*dim, j*dim:(j+1)*dim]
        col1 = (img_ij < 125).sum()
        col2 = (img_ij > 125).sum()
        img_ratios.append(min(col1, col2) / max(col1, col2))
    return np.array(img_ratios)


def get_loss(new_ratios, orig_ratios):
    # max_ratios = np.maximum(new_ratios, orig_ratios)
    # min_ratios = np.minimum(new_ratios, orig_ratios)
    idxs = np.where(orig_ratios != 0)
    if len(orig_ratios[idxs]) == 0:
        return 1.0
    return max(1 - new_ratios[idxs] / orig_ratios[idxs])


def transformations(img, orig_ratios_gray=None, n_transforms=0, max_transforms=None, n_images=60,
                    is_bubble_font=False, n_windows=10, transforms=()):
    if n_transforms > 0 and n_transforms >= max_transforms:
        return [(img, transforms)]
    fns = [inverse, dialated, bloated, bloated2, erode, eroded2, erode,  erode,
           erode, erode, erode]
    if n_transforms == 0:
        img = gray(img)
        if is_bubble_font:
            img = bubble_font(img, (7, 7))
        orig_ratios_gray = get_ratios(img, n_windows)
    imgs = []
    nbranches = 1 if n_transforms != 0 else n_images

    max_n_transforms = 7 if not is_bubble_font else 1
    max_k = 3 if not is_bubble_font else 1
    max_transforms_list = random.choices(range(max_n_transforms), k=nbranches)
    for idx in range(nbranches):
        ks = random.randint(1, max_k)
        trimgs = []
        chosen_transform = fns[random.randint(0, len(fns) - 1)]
        if n_transforms == 0:
            trimgs.append(chosen_transform(img, (ks, ks)))
        else:
            trimgs.append(chosen_transform(img, (ks, ks)))

        new_transforms = transforms + tuple([(chosen_transform.__name__, ks)])
        for trimg in trimgs:
            further_transformed = transformations(trimg, orig_ratios_gray,
                                                  n_transforms + 1, max_transforms_list[idx],
                                                  n_images, is_bubble_font, n_windows, new_transforms)
            for ele in further_transformed:
                trimg_heir, img_transforms = ele[0], ele[1]
                new_ratios = get_ratios(trimg_heir, n_windows=n_windows)
                loss = get_loss(new_ratios, orig_ratios_gray)
                # print(loss, img_transforms)
                # if get_loss(new_ratios, orig_ratios_gray) < 0.93:
                if len(trimg_heir.shape) == 2 or trimg_heir.shape[2] == 1:
                    trimg_heir = cv2.cvtColor(trimg_heir, cv2.COLOR_GRAY2RGB)
                trimg_heir_bin = binarize(trimg_heir, is_bubble_font)
                # if len(img.shape) == 2:
                #     img1 = cv2.cvtColor(img, cv2.COLOR_GRAY2RGB)
                # else:
                #     img1 = img
                # show_image(np.hstack([img1, trimg_heir_bin]))
                imgs.append((trimg_heir_bin, img_transforms))
                # else:
                #     if len(img.shape) == 2:
                #         img = cv2.cvtColor(img, cv2.COLOR_GRAY2RGB)
                #     imgs.append((img, ()))

    return imgs


def generate_augmented_logos(df_train, logo_dir, exp, test=False):
    df_train[['xmin', 'ymin', 'xmax', 'ymax']] = df_train[['xmin', 'ymin', 'xmax', 'ymax']].astype(int)
    for sup, dfg in tqdm(df_train.groupby('supplier')):
        for idx, row in dfg.iterrows():
            img = cv2.imread(logo_dir + '/detection/{}/train/images/'.format(exp) + row['filename'])
            logo_img = img[row['ymin']:row['ymax'], row['xmin']:row['xmax'], :]

            if test:
                logo_transforms = transformations(logo_img, n_images=10)
                for i in range(len(logo_transforms)//6 + 1):
                    img_list = logo_transforms[i*6:(i+1)*6]
                    if img_list:
                        logos = np.hstack(img_list)
                        show_image(logos)
            else:
                if not os.path.exists(logo_dir + '/classification/images/' + row['supplier']):
                    os.makedirs(logo_dir + '/classification/images/' + row['supplier'])

                logo_transforms = transformations(logo_img, orig_ratios_gray=None, n_transforms=0, max_transforms=None,
                                n_images=int(100//dfg.shape[0]) + 1, is_bubble_font=False, n_windows=20)
                logo_transforms += transformations(logo_img, orig_ratios_gray=None, n_transforms=0, max_transforms=None,
                                n_images=int(20//dfg.shape[0]) + 1, is_bubble_font=True, n_windows=20)
                for ii, new_logo in enumerate(logo_transforms):
                    # rshp_logo = reshape_logo(new_logo[0])
                    cv2.imwrite(logo_dir + '/classification/images/' + row['supplier'] + "/logo_{}.png".format(ii),
                                new_logo[0])


def download_supplier_images(input_df, download_dir=None, max_page_index=1):
    if not os.path.exists(download_dir):
        os.makedirs(download_dir)

    for _, row in input_df.iterrows():
        if row.page_index <= max_page_index:
            image_name = str(int(row.customer_id)) + '_' + str(int(row.invoice_id)) + '_' + \
                         str(int(row.page_index)) + '.png'
            sup_dir = os.path.join(download_dir, row['supplier_name'])
            if not os.path.exists(sup_dir):
                os.makedirs(sup_dir)
            print(sup_dir+'/'+image_name)
            image_path = os.path.join(sup_dir, image_name)
            # locator = S3ArtifactLocator(uri=row.page_pixels_location)
            if not os.path.exists(image_path):
                s3 = boto3.client('s3')
                lst = row.page_pixels_location.split(r'/')
                s3.download_file(lst[2], '/'.join(lst[3:]), image_path)


